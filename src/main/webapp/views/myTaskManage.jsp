<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jquery-easyui-1.3.3/themes/default/easyui.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/jquery-easyui-1.3.3/themes/icon.css">
<script type="text/javascript"
	src="${pageContext.request.contextPath}/jquery-easyui-1.3.3/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/jquery-easyui-1.3.3/jquery.easyui.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/jquery-easyui-1.3.3/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript">
	var url;
	function searchTitle() {
		$("#dg").datagrid('load', {
			"title" : $("#s_title").val()
		});
	}

	function deleteUser() {
		var selectedRows = $("#dg").datagrid('getSelections');
		if (selectedRows.length == 0) {
			$.messager.alert("系统提示", "请选择要删除的数据！");
			return;
		}
		var strIds = [];
		for (var i = 0; i < selectedRows.length; i++) {
			strIds.push(selectedRows[i].id);
		}
		var ids = strIds.join(",");
		$.messager.confirm("系统提示", "您确认要删除这<font color=red>"
				+ selectedRows.length + "</font>条数据吗？", function(r) {
			if (r) {
				$.post("${pageContext.request.contextPath}/task/delete.do", {
					ids : ids
				}, function(result) {
					if (result.success) {
						$.messager.alert("系统提示", "数据已成功删除！");
						$("#dg").datagrid("reload");
					} else {
						$.messager.alert("系统提示", "数据删除失败！");
					}
				}, "json");
			}
		});

	}

	function openUserAddDialog() {
		$("#dlg").dialog("open").dialog("setTitle", "添加任务信息");
		getDepts(null);
		url = "${pageContext.request.contextPath}/task/save.do";
	}

	function saveTask() {
		$("#fm").form("submit", {
			url : url,
			onSubmit : function() {
				return $(this).form();
			},
			success : function(result) {
				$.messager.alert("系统提示", "保存成功");
				resetValue();
				$("#dlg").dialog("close");
				$("#dg").datagrid("reload");
			}
		});
	}

	function openUserModifyDialog() {
		var selectedRows = $("#dg").datagrid('getSelections');
		if (selectedRows.length != 1) {
			$.messager.alert("系统提示", "请选择一条要编辑的数据！");
			return;
		}
 		var row = selectedRows[0];
 		getDepts(row);
		$("#dlg").dialog("open").dialog("setTitle", "编辑任务信息");
		$('#fm').form('load', row);
		url = "${pageContext.request.contextPath}/task/save.do?id=" + row.id;
	}

	function resetValue() {
		$("#title").val("");
		$("#content").val("");
		$("#progress").val("");
		$("#puser.name").val("");
		$("#guser.name").val("");
		$("#rank.name").val("");
		$("#pubTime").val("");
		$("#ecpectTime").val("");
		$("#finishTime").val("");
	}

	function closeUserDialog() {
		$("#dlg").dialog("close");
		resetValue();
	}

	//查出所有的部门信息并显示在下拉列表中
	function getDepts(row) {
//  		alert("=====" + row.title + 
//  				"  " + row.content  
//  				 + "  " + row.progress
//  				 + "  " + row.puser.name
//  				 + "  " + row.guser.name
//  				 + "  " + row.rank.name
//  				 + "  " + row.pubTime
//  				 + "  " + row.ecpectTime
//  				 + "  " + row.finishTime);
// 		$("#progress").empty();
// 		$("#puser.name").empty();
// 		$("#guser.name").empty();
// 		$("#rank.name").empty();
		
		$.get("${pageContext.request.contextPath}/task/allUserRank.do",function(result) {
			if (result) {
				var user = result.user;
				var rank = result.rank;
				var progress = result.progress;
				var strp = "";
				var strg = "";
				var rankStr = "";
				var progressStr = "";
 				for(var i = 0 ; i < user.length ; i++){
 					strp += "<option value='"+user[i].id + "'";
 					if(row!=null&&row.puser.name == user[i].name){
 						strp +=" Selected ";
 					}
 					strp+=">"+user[i].name+"</option>";
 					
 					strg += "<option value='"+user[i].id + "'";
 					if(row!=null&&row.guser.name == user[i].name){
 						strg +=" Selected ";
 					}
 					strg+=">"+user[i].name+"</option>";
 				}
 				for(var i = 0 ; i < rank.length ; i++){
 					rankStr += "<option value='"+rank[i].id + "'";
 					if(row!=null&&row.rank.name == rank[i].name){
 						rankStr +=" Selected ";
 					}
 					rankStr+=">"+rank[i].name+"</option>";
 					
 				}
 				for(var i = 0 ; i < progress.length ; i++){
 					progressStr += "<option value='"+progress[i].id + "'";
 					if(row!=null&&row.progress.name == progress[i].name){
 						progressStr +=" Selected ";
 					}
 					progressStr+=">"+progress[i].name+"</option>";
 					
 				}
 				$("#pubUserId").html(strp);  
 				$("#getUserId").html(strg);  
 				$("#rankId").html(rankStr);  
 				$("#progressId").html(progressStr);  
			} 
		}, "json");
	}
</script>
</head>
<body style="margin: 1px;">
	<table id="dg" title="任务管理" class="easyui-datagrid" fitColumns="true"
		pagination="true" rownumbers="true"
		url="${pageContext.request.contextPath}/task/mylist.do" fit="true"
		toolbar="#tb">
		<thead>
			<tr>
				<th field="cb" checkbox="true" align="center"></th>
        		<th field="id" width="70" align="center">任务编号</th>
        		<th field="title" width="500" align="center">任务标题</th>
        		<th field="content" width="500" align="center">任务内容</th>
        		<th field="progress.name" width="70" align="center">完成进度</th>
        		<th field="puser.name" width="70" align="center">发布人</th>
        		<th field="guser.name" width="70" align="center">指向人</th>
        		<th field="rank.name" width="70" align="center">任务级别</th>
        		<th field="pubTime" width="100" align="center">发布时间</th>
        		<th field="ecpectTime" width="100" align="center">预计完成时间</th>
        		<th field="finishTime" width="100" align="center">完成时间</th>
			</tr>
		</thead>
	</table>
	<div id="tb">
		<div>
			<a href="javascript:openUserAddDialog()" class="easyui-linkbutton"
				iconCls="icon-add" plain="true">添加</a> <a
				href="javascript:openUserModifyDialog()" class="easyui-linkbutton"
				iconCls="icon-edit" plain="true">修改</a> <a
				href="javascript:deleteUser()" class="easyui-linkbutton"
				iconCls="icon-remove" plain="true">删除</a>
		</div>
		<div>
			&nbsp;任务标题：&nbsp;<input type="text" id="s_title" size="20"
				onkeydown="if(event.keyCode==13) searchTitle()" /> <a
				href="javascript:searchTitle()" class="easyui-linkbutton"
				iconCls="icon-search" plain="true">搜索</a>
		</div>
	</div>

	<div id="dlg" class="easyui-dialog"
		style="width: 800px; height: 500px; padding: 10px 20px" closed="true"
		buttons="#dlg-buttons">
		<form id="fm" method="post">
			<table cellspacing="8px">
<!-- 			
<th field="cb" checkbox="true" align="center"></th>
<th field="id" width="70" align="center">任务编号</th>
<th field="title" width="500" align="center">任务标题</th>
<th field="progress" width="70" align="center">完成进度</th>
<th field="puser.name" width="70" align="center">发布人</th>
<th field="guser.name" width="70" align="center">指向人</th>
<th field="rank.name" width="70" align="center">任务级别</th>
<th field="pubTime" width="100" align="center">发布时间</th>
<th field="ecpectTime" width="100" align="center">预计完成时间</th>
<th field="finishTime" width="100" align="center">完成时间</th>
	$("#title").val("");
		$("#progress").val("");
		$("#puser.name").val("");
		$("#guser.name").val("");
		$("#rank.name").val("");
		$("#pubTime").val("");
		$("#ecpectTime").val("");
		$("#finishTime").val("");
 -->
				<tr>
					<td>任务标题：</td>
					<td><input type="text" id="title" name="title"	style="width: 600px;"
						class="easyui-validatebox" required="true" />&nbsp;<font
						color="red">*</font></td>
				</tr>
				<tr>
					<td>任务内容：</td>
					<td>
					<textarea id="content" name="content" cols="92" rows="6" required></textarea>
					&nbsp;<font color="red">*</font>
				
				</tr>
				<tr>
					<td>发布人：</td>
					<td><select id="pubUserId" name="pubUserId" required="true" class="easyui-validatebox">
					</select>
					&nbsp;<font color="red">*</font>
					</td>
				</tr>
				<tr>
					<td>任务进度：</td>
					<td><select id="progressId" name="progressId" class="easyui-validatebox" required="true">
					</select>&nbsp;<font color="red">*</font></td>
				</tr>
				<tr>
					<td>指向人：</td>
					<td><select id="getUserId" name="getUserId" required="true" class="easyui-validatebox">
					</select>
					&nbsp;<font color="red">*</font>
					</td>
				</tr>
				<tr>
					<td>任务级别：</td>
					<td><select id="rankId" name="rankId" required="true" class="easyui-validatebox">
					</select>
					&nbsp;<font color="red">*</font>
					</td>
				</tr>
				<tr> 
 				    <td>预计完成时间:</td> 
 				    <td><input id="ecpectTime" name="ecpectTime" class="easyui-datetimebox"></td>
 				</tr> 
 				<tr> 
 				    <td>实际完成时间:</td> 
 				    <td><input id="finishTime" name="finishTime"  class="easyui-datetimebox"></td>
				</tr> 
			</table>
		</form>
	</div>

	<div id="dlg-buttons">
		<a href="javascript:saveTask()" class="easyui-linkbutton"
			iconCls="icon-ok">保存</a> <a href="javascript:closeUserDialog()"
			class="easyui-linkbutton" iconCls="icon-cancel">关闭</a>
	</div>
</body>
</html>