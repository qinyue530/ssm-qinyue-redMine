package com.qy.ssm.test;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.qy.ssm.bean.Task;
import com.qy.ssm.dao.DepartmentMapper;
import com.qy.ssm.dao.ProgressMapper;
import com.qy.ssm.dao.TaskMapper;
import com.qy.ssm.service.TaskService;
import com.qy.ssm.utils.MD5Util;

/**
 * 测试dao层的工作
 * @author lfy
 *推荐Spring的项目就可以使用Spring的单元测试，可以自动注入我们需要的组件
 *1、导入SpringTest模块
 *2、@ContextConfiguration指定Spring配置文件的位置
 *3、直接autowired要使用的组件即可
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:applicationContext.xml"})
public class MapperTest {
	
	@Autowired
	TaskMapper taskMapper;
	@Autowired
	ProgressMapper progressMapper;
	@Autowired
	DepartmentMapper departmentMapper;
	@Autowired
	TaskService taskService;
	/**
	 * 测试DepartmentMapper
	 */
	@Test
	public void testCRUD(){
	/*	//1、创建SpringIOC容器
		ApplicationContext ioc = new ClassPathXmlApplicationContext("applicationContext.xml");
		//2、从容器中获取mapper
		DepartmentMapper bean = ioc.getBean(DepartmentMapper.class);*/
//		User user =  userMapper.selectByPrimaryKey("admin");
//		System.out.println(user.toString());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String MD5pwd = MD5Util.MD5Encode("123456", "UTF-8");
		Task task = new Task();
		task.setPubUserId(1);
		task.setGetUserId(1);
		Date now = new Date();
		
    	String time = sdf.format(now);
		task.setPubTime();
		task.setProgressId(1);
		task.setFinishTime(time);
		task.setEcpectTime(time);
		task.setRankId(1);
		task.setTitle("88888");
		task.setContent("88888");
//		task.setId(2);
		System.out.println(task.toString());
		taskMapper.addTask(task);
		
//		System.out.println("==================================");
//		List<Progress> progress = progressMapper.allProgress();
//		for(Progress a : progress) {
//			System.out.println(a.toString());
//		}
		//1、插入几个部门
	//	departmentMapper.insertSelective(new Department(null, "开发部"));
	//	departmentMapper.insertSelective(new Department(null, "测试部"));
		
		//2、生成员工数据，测试员工插入
	//	employeeMapper.insertSelective(new Employee(null, "Jerry", "M", "Jerry@atguigu.com", 1));
		
		//3、批量插入多个员工；批量，使用可以执行批量操作的sqlSession。
		
//		EmployeeMapper mapper = sqlSession.getMapper(EmployeeMapper.class);
//		for(int i = 0;i<1000;i++){
//			String uid = UUID.randomUUID().toString().substring(0,5)+i;
//			mapper.insertSelective(new Employee(null,uid, "M", uid+"@qinyue.com", 1));
//		}
//		System.out.println("批量完成");
		
	}

}
