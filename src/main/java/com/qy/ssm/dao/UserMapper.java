package com.qy.ssm.dao;

import com.qy.ssm.bean.User;
import com.qy.ssm.bean.UserExample;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface UserMapper {
	
	long countByExample(UserExample example);

	int deleteByExample(UserExample example);

	int deleteByPrimaryKey(String userName);

	int insert(User record);

	int insertSelective(User record);

	List<User> selectByExample(UserExample example);

	User selectByPrimaryKey(String userName);

	int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);

	int updateByExample(@Param("record") User record, @Param("example") UserExample example);

	int updateByPrimaryKeySelective(User record);

	int updateByPrimaryKey(User record);
	
	User login(User user);

	List<User> findUsers(Map<String, Object> map);

	Long getTotalUser(Map<String, Object> map);
	
	int updateUser(User user);

	int addUser(User user);

	int deleteUser(Integer id);
	
	List<User> allUser();
}