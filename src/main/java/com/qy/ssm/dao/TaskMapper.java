package com.qy.ssm.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.qy.ssm.bean.Task;
import com.qy.ssm.bean.TaskExample;

public interface TaskMapper {
    long countByExample(TaskExample example);

    int deleteByExample(TaskExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Task record);

    int insertSelective(Task record);

    List<Task> selectByExample(TaskExample example);

    Task selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Task record, @Param("example") TaskExample example);

    int updateByExample(@Param("record") Task record, @Param("example") TaskExample example);

    int updateByPrimaryKeySelective(Task record);

    int updateByPrimaryKey(Task record);
    
    List<Task> allTask();
    
    Long getTotalTask(Map<String, Object> map);
    
    int updateTask(Task task);

	int addTask(Task task);

	int deleteTask(Integer id);
	
	List<Task> findTasks(Map<String, Object> map);
	List<Task> findTasksNo(Map<String, Object> map);
	List<Task> findTasksYes(Map<String, Object> map);
	
	List<Task> myfindTasks(Map<String, Object> map);
	List<Task> myfindTasksNo(Map<String, Object> map);
	List<Task> myfindTasksYes(Map<String, Object> map);
}