package com.qy.ssm.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class testTask {
	public  int i = 0;
	@Scheduled(cron = "0/5 * * * * ? ") // 间隔5秒执行
	public void taskCycle() {

//		System.out.println("使用SpringMVC框架配置定时任务 = " + i++);
	}
}