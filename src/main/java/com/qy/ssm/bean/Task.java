package com.qy.ssm.bean;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Task {
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
    private Integer id;

    private Integer pubUserId;

    private Integer getUserId;

    private String pubTime;

    private Integer progressId;

    private String finishTime;

    private String ecpectTime;

    private Integer rankId;

    private String title;

    private String content;
    
    private Rank rank;
    private User puser;
    private User guser;
    private Progress progress;

    @Override
	public String toString() {
		return "Task [id=" + id + ", pubUserId=" + pubUserId + ", getUserId=" + getUserId + ", pubTime=" + pubTime
				+ ", progressId=" + progressId + ", finishTime=" + finishTime + ", ecpectTime=" + ecpectTime
				+ ", rankId=" + rankId + ", title=" + title + ", content=" + content + ", rank=" + rank + ", puser="
				+ puser + ", guser=" + guser + ", progress=" + progress + "]";
	}

	public Progress getProgress() {
		return progress;
	}

	public void setProgress(Progress progress) {
		this.progress = progress;
	}

	public Rank getRank() {
		return rank;
	}

	public void setRank(Rank rank) {
		this.rank = rank;
	}

	public User getPuser() {
		return puser;
	}

	public void setPuser(User puser) {
		this.puser = puser;
	}

	public User getGuser() {
		return guser;
	}

	public void setGuser(User guser) {
		this.guser = guser;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPubUserId() {
        return pubUserId;
    }

    public void setPubUserId(Integer pubUserId) {
        this.pubUserId = pubUserId;
    }

    public Integer getGetUserId() {
        return getUserId;
    }

    public void setGetUserId(Integer getUserId) {
        this.getUserId = getUserId;
    }

    public String getPubTime() {
        return pubTime;
    }

    public void setPubTime() {
    	Date now = new Date();
    	String time = sdf.format(now);
        this.pubTime = time;
    }

    public Integer getProgressId() {
        return progressId;
    }

    public void setProgressId(Integer progressId) {
        this.progressId = progressId;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime == null ? null : finishTime.trim();
    }

    public String getEcpectTime() {
        return ecpectTime;
    }

    public void setEcpectTime(String ecpectTime) {
        this.ecpectTime = ecpectTime == null ? null : ecpectTime.trim();
    }

    public Integer getRankId() {
        return rankId;
    }

    public void setRankId(Integer rankId) {
        this.rankId = rankId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}