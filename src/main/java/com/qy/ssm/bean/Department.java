package com.qy.ssm.bean;

public class Department {
    private Integer id;

    private String department;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department == null ? null : department.trim();
    }

	@Override
	public String toString() {
		return "Department [id=" + id + ", department=" + department + "]";
	}
}