package com.qy.ssm.bean;

public class Rank {
    private Integer id;

    private String name;

    public Integer getId() {
        return id;
    }

    @Override
	public String toString() {
		return "Rank [id=" + id + ", name=" + name + "]";
	}

	public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }
}