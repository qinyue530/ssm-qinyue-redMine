package com.qy.ssm.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qy.ssm.bean.Task;
import com.qy.ssm.dao.TaskMapper;

@Service
public class TaskService {
	
	@Autowired
	TaskMapper taskMapper;

	public List<Task> allTask() {
		return taskMapper.allTask();
	}
	public Long getTotalTask(Map<String, Object> map) {
		return taskMapper.getTotalTask(map);
	}
}
