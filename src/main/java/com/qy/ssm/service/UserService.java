package com.qy.ssm.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qy.ssm.bean.User;
import com.qy.ssm.bean.UserExample;
import com.qy.ssm.dao.UserMapper;

@Service
public class UserService {

	@Autowired
	private UserMapper userMapper;

	public User login(User user) {
		return userMapper.login(user);
	}

	public int updateUser(User user) {
		// 防止有人胡乱修改导致其他人无法正常登陆
//		if ("admin".equals(user.getUserName())) {
//			return 0;
//		}
		return userMapper.updateUser(user);
	}

	public Long getTotalUser(Map<String, Object> map) {
		return userMapper.getTotalUser(map);
	}

	public int addUser(User user) {
		if (user.getUserName() == null || user.getPassword() == null || user.getDepartmentId() == null
				|| user.getName() == null) {
			return 0;
		}
		return userMapper.addUser(user);
	}

	public List<User> findUser(Map<String, Object> map) {
		return userMapper.findUsers(map);
	}

	public List<User> allUser() {
		return userMapper.allUser();
	}
	
	public int deleteUser(Integer id) {
		// 防止有人胡乱修改导致其他人无法正常登陆
		if (0 == id) {
			return 0;
		}
		return userMapper.deleteUser(id);
	}

}
