package com.qy.ssm.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.qy.ssm.bean.PageBean;
import com.qy.ssm.bean.Progress;
import com.qy.ssm.bean.Rank;
import com.qy.ssm.bean.Task;
import com.qy.ssm.bean.User;
import com.qy.ssm.dao.ProgressMapper;
import com.qy.ssm.dao.RankMapper;
import com.qy.ssm.dao.TaskMapper;
import com.qy.ssm.service.UserService;
import com.qy.ssm.utils.ResponseUtil;
import com.qy.ssm.utils.StringUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/task")
public class TaskController {
	@Resource
	private TaskMapper taskMapper;
	@Resource
	private UserService userService;
	@Resource
	private RankMapper rankMapper;
	@Resource
	private ProgressMapper progressMapper;
	private static final Logger log = Logger.getLogger(UserController.class);// 日志文件

	@RequestMapping("/list")
	public String list(@RequestParam(value = "page", required = false) String page,
			@RequestParam(value = "rows", required = false) String rows, Task s_task, HttpServletResponse response)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if (page != null && rows != null) {
			PageBean pageBean = new PageBean(Integer.parseInt(page), Integer.parseInt(rows));
			map.put("start", pageBean.getStart());
			map.put("size", pageBean.getPageSize());
		}
		map.put("title", StringUtil.formatLike(s_task.getTitle()));
		List<Task> taskList = taskMapper.findTasks(map);
		Long total = taskMapper.getTotalTask(map);
		JSONObject result = new JSONObject();
		JSONArray jsonArray = JSONArray.fromObject(taskList);
		result.put("rows", jsonArray);
		result.put("total", total);
		log.info("request: allTask/list , map: " + map.toString());
		ResponseUtil.write(response, result);
		return null;
	}

	@RequestMapping("/allUserRank")
	public String allDepts(HttpServletResponse response) throws Exception {
		List<User> listUser = userService.allUser();
		List<Rank> listRank = rankMapper.allRank();
		List<Progress> listProgress = progressMapper.allProgress();
		JSONObject result = new JSONObject();
		JSONArray jsonArrayUser = JSONArray.fromObject(listUser);
		JSONArray jsonArrayRank = JSONArray.fromObject(listRank);
		JSONArray jsonArrayProgress = JSONArray.fromObject(listProgress);
		result.put("user", jsonArrayUser);
		result.put("rank", jsonArrayRank);
		result.put("progress", jsonArrayProgress);
		ResponseUtil.write(response, result);
		return null;
	}

	@RequestMapping("/save")
	public String save(Task task, HttpServletResponse response) throws Exception {
		int resultTotal = 0;
		System.out.println("====================="+task.toString());
		log.info("修改的任务信息 ：" + task.toString());
		if (task.getId() == null) {
			task.setPubTime();
			resultTotal = taskMapper.addTask(task);
		} else {
			resultTotal = taskMapper.updateTask(task);
		}
		JSONObject result = new JSONObject();
		if (resultTotal > 0) {
			result.put("success", true);
		} else {
			result.put("success", false);
		}
		result.put("success", true);
		log.info("request: task/save , task: " + task.toString());
		ResponseUtil.write(response, result);
		return null;
	}
	/**
	 * 删除任务
	 *
	 * @param ids
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/delete")
	public String delete(@RequestParam(value = "ids") String ids, HttpServletResponse response) throws Exception {
		JSONObject result = new JSONObject();
		String[] idsStr = ids.split(",");
		for (int i = 0; i < idsStr.length; i++) {
			taskMapper.deleteTask(Integer.parseInt(idsStr[i]));
		}
		result.put("success", true);
		log.info("request: task/delete , ids: " + ids);
		ResponseUtil.write(response, result);
		return null;
	}
	
	@RequestMapping("/listyes")
	public String listYes(@RequestParam(value = "page", required = false) String page,
			@RequestParam(value = "rows", required = false) String rows, Task s_task, HttpServletResponse response)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if (page != null && rows != null) {
			PageBean pageBean = new PageBean(Integer.parseInt(page), Integer.parseInt(rows));
			map.put("start", pageBean.getStart());
			map.put("size", pageBean.getPageSize());
		}
		map.put("title", StringUtil.formatLike(s_task.getTitle()));
		List<Task> taskList = taskMapper.findTasksYes(map);
		Long total = taskMapper.getTotalTask(map);
		JSONObject result = new JSONObject();
		JSONArray jsonArray = JSONArray.fromObject(taskList);
		result.put("rows", jsonArray);
		result.put("total", total);
		log.info("request: allTask/list , map: " + map.toString());
		ResponseUtil.write(response, result);
		return null;
	}
	
	@RequestMapping("/listno")
	public String listNo(@RequestParam(value = "page", required = false) String page,
			@RequestParam(value = "rows", required = false) String rows, Task s_task, HttpServletResponse response)
			throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if (page != null && rows != null) {
			PageBean pageBean = new PageBean(Integer.parseInt(page), Integer.parseInt(rows));
			map.put("start", pageBean.getStart());
			map.put("size", pageBean.getPageSize());
		}
		map.put("title", StringUtil.formatLike(s_task.getTitle()));
		List<Task> taskList = taskMapper.findTasksNo(map);
		Long total = taskMapper.getTotalTask(map);
		JSONObject result = new JSONObject();
		JSONArray jsonArray = JSONArray.fromObject(taskList);
		result.put("rows", jsonArray);
		result.put("total", total);
		log.info("request: allTask/list , map: " + map.toString());
		ResponseUtil.write(response, result);
		return null;
	}
	
	@RequestMapping("/mylistyes")
	public String mylistYes(@RequestParam(value = "page", required = false) String page,
			@RequestParam(value = "rows", required = false) String rows, Task s_task, HttpServletResponse response)
			throws Exception {
		HttpSession session= ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest().getSession();
		User my = (User)session.getAttribute("currentUser");
		Map<String, Object> map = new HashMap<String, Object>();
		if (page != null && rows != null) {
			PageBean pageBean = new PageBean(Integer.parseInt(page), Integer.parseInt(rows));
			map.put("start", pageBean.getStart());
			map.put("size", pageBean.getPageSize());
		}
		map.put("title", StringUtil.formatLike(s_task.getTitle()));
		map.put("mainUserId", my.getId());
		List<Task> taskList = taskMapper.myfindTasksYes(map);
		Long total = taskMapper.getTotalTask(map);
		JSONObject result = new JSONObject();
		JSONArray jsonArray = JSONArray.fromObject(taskList);
		result.put("rows", jsonArray);
		result.put("total", total);
		log.info("request: allTask/list , map: " + map.toString());
		ResponseUtil.write(response, result);
		return null;
	}
	
	@RequestMapping("/mylistno")
	public String mylistNo(@RequestParam(value = "page", required = false) String page,
			@RequestParam(value = "rows", required = false) String rows, Task s_task, HttpServletResponse response)
			throws Exception {
		HttpSession session= ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest().getSession();
		User my = (User)session.getAttribute("currentUser");
		Map<String, Object> map = new HashMap<String, Object>();
		if (page != null && rows != null) {
			PageBean pageBean = new PageBean(Integer.parseInt(page), Integer.parseInt(rows));
			map.put("start", pageBean.getStart());
			map.put("size", pageBean.getPageSize());
		}
		map.put("title", StringUtil.formatLike(s_task.getTitle()));
		map.put("mainUserId", my.getId());
		List<Task> taskList = taskMapper.myfindTasksNo(map);
		Long total = taskMapper.getTotalTask(map);
		JSONObject result = new JSONObject();
		JSONArray jsonArray = JSONArray.fromObject(taskList);
		result.put("rows", jsonArray);
		result.put("total", total);
		log.info("request: allTask/list , map: " + map.toString());
		ResponseUtil.write(response, result);
		return null;
	}
	
	@RequestMapping("/mylist")
	public String mylist(@RequestParam(value = "page", required = false) String page,
			@RequestParam(value = "rows", required = false) String rows, Task s_task, HttpServletResponse response)
			throws Exception {
		HttpSession session= ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest().getSession();
		User my = (User)session.getAttribute("currentUser");
		Map<String, Object> map = new HashMap<String, Object>();	
		if (page != null && rows != null) {
			PageBean pageBean = new PageBean(Integer.parseInt(page), Integer.parseInt(rows));
			map.put("start", pageBean.getStart());
			map.put("size", pageBean.getPageSize());
		}
		map.put("title", StringUtil.formatLike(s_task.getTitle()));
		map.put("mainUserId", my.getId());
		List<Task> taskList = taskMapper.myfindTasks(map);
		Long total = taskMapper.getTotalTask(map);
		JSONObject result = new JSONObject();
		JSONArray jsonArray = JSONArray.fromObject(taskList);
		result.put("rows", jsonArray);
		result.put("total", total);
		log.info("request: allTask/list , map: " + map.toString());
		ResponseUtil.write(response, result);
		return null;
	}

}
