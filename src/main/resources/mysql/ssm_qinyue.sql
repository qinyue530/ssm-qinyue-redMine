/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : localhost:3306
 Source Schema         : ssm_qinyue

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : 65001

 Date: 17/03/2019 23:35:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for qinyue_department
-- ----------------------------
DROP TABLE IF EXISTS `qinyue_department`;
CREATE TABLE `qinyue_department`  (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `department` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qinyue_department
-- ----------------------------
INSERT INTO `qinyue_department` VALUES (1, '服务器');
INSERT INTO `qinyue_department` VALUES (2, '客户端');

-- ----------------------------
-- Table structure for qinyue_log
-- ----------------------------
DROP TABLE IF EXISTS `qinyue_log`;
CREATE TABLE `qinyue_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NULL DEFAULT NULL,
  `log` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qinyue_progress
-- ----------------------------
DROP TABLE IF EXISTS `qinyue_progress`;
CREATE TABLE `qinyue_progress`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qinyue_progress
-- ----------------------------
INSERT INTO `qinyue_progress` VALUES (1, '0%');
INSERT INTO `qinyue_progress` VALUES (2, '10%');
INSERT INTO `qinyue_progress` VALUES (3, '20%');
INSERT INTO `qinyue_progress` VALUES (4, '30%');
INSERT INTO `qinyue_progress` VALUES (5, '40%');
INSERT INTO `qinyue_progress` VALUES (6, '50%');
INSERT INTO `qinyue_progress` VALUES (7, '60%');
INSERT INTO `qinyue_progress` VALUES (8, '70%');
INSERT INTO `qinyue_progress` VALUES (9, '80%');
INSERT INTO `qinyue_progress` VALUES (10, '90%');
INSERT INTO `qinyue_progress` VALUES (11, '100%');

-- ----------------------------
-- Table structure for qinyue_rank
-- ----------------------------
DROP TABLE IF EXISTS `qinyue_rank`;
CREATE TABLE `qinyue_rank`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qinyue_rank
-- ----------------------------
INSERT INTO `qinyue_rank` VALUES (1, '普通');
INSERT INTO `qinyue_rank` VALUES (2, '一般');
INSERT INTO `qinyue_rank` VALUES (3, '中等');
INSERT INTO `qinyue_rank` VALUES (4, '重要');
INSERT INTO `qinyue_rank` VALUES (5, '紧急');

-- ----------------------------
-- Table structure for qinyue_task
-- ----------------------------
DROP TABLE IF EXISTS `qinyue_task`;
CREATE TABLE `qinyue_task`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pub_user_id` int(11) NOT NULL,
  `get_user_id` int(11) NOT NULL,
  `pub_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `progress_id` int(11) NOT NULL,
  `finish_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ecpect_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `rank_id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qinyue_task
-- ----------------------------
INSERT INTO `qinyue_task` VALUES (1, 1, 1, '2019-03-17 11:29:29', 11, '2019-03-17 11:29:29', '2019-03-17 11:29:29', 1, '88888', '88888');
INSERT INTO `qinyue_task` VALUES (2, 1, 1, '2019-03-17 11:35:07', 1, '', '', 1, '111', '111');
INSERT INTO `qinyue_task` VALUES (3, 2, 2, '2019-03-17 11:36:24', 1, '', '', 1, 'aa', 'aa');

-- ----------------------------
-- Table structure for qinyue_user
-- ----------------------------
DROP TABLE IF EXISTS `qinyue_user`;
CREATE TABLE `qinyue_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `department_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qinyue_user
-- ----------------------------
INSERT INTO `qinyue_user` VALUES (1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 1, 'admin');
INSERT INTO `qinyue_user` VALUES (2, 'a', '0cc175b9c0f1b6a831c399e269772661', 1, 'a');

SET FOREIGN_KEY_CHECKS = 1;
